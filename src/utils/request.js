// 目标： 封装axios自定义工具函数，负责整个项目网络请求发起和接收
// 基于axios进行二次封装
import axios from 'axios'

// axios.create()创建自定义axios函数，比直接引入的，可以多设置一些配置项
const instance = axios.create({
  // 什么叫基准地址   baseURL + url拼接后 发送请求
  baseURL: 'http://geek.itheima.net'
})
// export default instance

// 中间用统一名字接收，函数如果使用，统一在这里处理
// 导出自定义函数, 参数对象解构赋值
// 好处：逻辑页面直接还用这五个名配置项传入，将来如果内部改变，不需要懂逻辑页面代码
// 只需要在这接收后，在函数内自己做处理即可（方便将来请求库的切换）
export default ({ url, method = 'GET', params = {}, data = {}, headers = {} }) => {
  return instance({
    url: url,
    method: method,
    params: params,
    data: data,
    headers: headers
  })
  // return Promise对象到逻辑页面去接收请求响应结果数据
  // retrun 到本次函数调用的地方
}
// 好处 ： 以后换库, 只需要改这里, 逻辑页面不用动, 保证代码的复用性和独立性(高内聚低耦合)
//   return $.ajax({
//     url: url,
//     type: method,
//     data: data,
//     header: headers
//   })
// }
