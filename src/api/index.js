// 封装接口请求的方法

// axios是request.js里封装自定义函数
import axios from '@/utils/request'
import store from '@/store'

/**
 * 获取所有频道
 * @returns promise对象
 */
export const getAllChangeListAPI = () => {
  return axios({
    url: '/v1_0/channels'
  })
}

// 函数内自己确定不了的值，让外部传入

/**
 *登录接口
 * @param {*} param0 {mobile:手机号, code:验证码}
 * @returns promise
 */
export const loginAPI = ({ mobile, code }) => {
  // axios的data选项如果是一个JS对象，axios内部会帮你转成JSON字符串
  return axios({
    url: '/v1_0/authorizations',
    method: 'POST',
    data: {
      mobile: mobile,
      code: code
    }
  })
}

// vuex在内存中，浏览器本地在磁盘上
/**
 * 获取-用户已选列表
 * @returns promise对象
 */
export const getUserChanneListlAPI = () => {
  return axios({
    url: '/v1_0/user/channels',
    headers: {
      // Authorization: 'Bearer ' + store.state.token // 这里爱忘记空格字符串带的空格
      Authorization: `Bearer ${store.state.token}` // 这里用模板字符串
    }
  })
}
