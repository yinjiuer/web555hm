import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { Button, NavBar, Form, Field, Notify, Tabbar, TabbarItem, Icon, Tab, Tabs } from 'vant'
import 'amfe-flexible' // 引入flexible.js (用于动态设置html的font-size)，让rem单位适配

Vue.use(Tab)
Vue.use(Tabs)
Vue.use(Icon)
Vue.use(Tabbar)
Vue.use(TabbarItem)

// main.js 是入口（先执行）
// Vue.use() 注册插件（全局）
Vue.use(Field)
Vue.use(Form)
Vue.use(NavBar)
Vue.use(Button)

Vue.prototype.$notify = Notify

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
