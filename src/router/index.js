import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/views/Layout'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: () => import('@/views/Login')
    // 路由懒加载：webpack提供import函数。可以在路由路径匹配后，再去引入对应的组件
  },
  {
    path: '/layout',
    component: Layout,
    redirect: '/layout/home', // 立刻做重定向
    children: [
      {
        path: 'home',
        component: () => import('@/views/Home')
      },
      {
        path: 'user',
        component: () => import('@/views/User')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
