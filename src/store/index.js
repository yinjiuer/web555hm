import Vue from 'vue'
import Vuex from 'vuex'
import { loginAPI } from '@/api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: { // 定义全局状态变量
    token: localStorage.getItem('token') || '', // 前面有值用前面的, 前面要是null, 就用后面的
    refresh_token: localStorage.getItem('refresh_token') || ''
  },
  getters: { // 类似全局计算属性
  },
  mutations: { // 是唯一能同步修改state变量的地方（同步：函数内只能是同步代码）
    SET_TOKEN (state, value) {
      state.token = value // 把逻辑页面传给我的token字符串保存到vuex的state变量中
      localStorage.setItem('tpken', value)
    },
    SET_REFRESH_TOKEN (state, value) {
      state.refresh_token = value
      localStorage.setItem('refresh_token', value)
    }

  },
  actions: { // 异步操作： 要调用mutations把结果传递，修改state变量
    async loginActions (store, value) { // 封装登录方法（便于登录逻辑复用）
      // value逻辑页面传递过来的{}手机号和验证码和值
      const res = await loginAPI(value)
      store.commit('SET_TOKEN', res.data.data.token)
      store.commit('SET_REFRESH_TOKEN', res.data.data.refresh_token)

      return res // return到逻辑页面当中(async函数始终返回的是一个promise对象，所以你的结果会被当做promise对象)
    }
  },
  modules: {
  }
})
